package com.example.taskmanagerreallyquick.repository

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.rx2.Rx2Apollo
import com.example.taskmanagerreallyquick.GenerateAccessTokenMutation
import io.reactivex.Observable

class UserRepositoryImpl(
    private val tokenProvider: TokenProvider,
    private val apolloClient: ApolloClient
) : UserRepository {

    override fun isLoggedIn(): Boolean {
        return tokenProvider.hasToken()
    }

    override fun createAccessToken(username: String): Observable<Any> {
        return Rx2Apollo.from(
            apolloClient.mutate(
                GenerateAccessTokenMutation.builder().userName(username).build()
            )
        )
            .doOnNext { tokenProvider.setToken(it.data()!!.generateAccessToken()!!) }
            .map { Any() }
    }
}