package com.example.taskmanagerreallyquick.repository

import io.reactivex.Observable

interface UserRepository {

    fun isLoggedIn(): Boolean

    fun createAccessToken(username: String): Observable<Any>

}