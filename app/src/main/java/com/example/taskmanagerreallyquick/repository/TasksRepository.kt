package com.example.taskmanagerreallyquick.repository

import com.example.taskmanagerreallyquick.model.Task
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

interface TasksRepository {

    fun getTasksIfNeeded(): Observable<List<Task>>

    fun createTask(name: String, notes: String): Observable<Any>

    fun updateTask(id: String, checked: Boolean): Observable<Any>

}