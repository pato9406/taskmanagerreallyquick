package com.example.taskmanagerreallyquick.repository

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.rx2.Rx2Apollo
import com.example.taskmanagerreallyquick.AllTasksQuery
import com.example.taskmanagerreallyquick.CreateTaskMutation
import com.example.taskmanagerreallyquick.UpdateTaskStatusMutation
import com.example.taskmanagerreallyquick.model.Task
import io.reactivex.Observable

class TasksRepositoryImpl(private val apolloClient: ApolloClient) : TasksRepository {

    private lateinit var lastTasks: List<Task>
    private var needsRefetch = true

    override fun getTasksIfNeeded(): Observable<List<Task>> {
        if (!needsRefetch) {
            return Observable.just(lastTasks)
        }

        val query = apolloClient.query(AllTasksQuery.builder().build())

        return Rx2Apollo.from(query)
            .map { it.data()!!.allTasks()!!.map(::toTask) }
            .doOnNext {
                lastTasks = it
                needsRefetch = false
            }
    }

    override fun createTask(name: String, notes: String): Observable<Any> {
        val mutation =
            apolloClient.mutate(CreateTaskMutation.builder().name(name).note(notes).isDone(false).build())

        return Rx2Apollo.from(mutation)
            .map { Any() }
            .doOnNext { needsRefetch = true }
    }

    override fun updateTask(id: String, checked: Boolean): Observable<Any> {
        val mutation =
            apolloClient.mutate(UpdateTaskStatusMutation.builder().taskId(id).isDone(checked).build())

        return Rx2Apollo.from(mutation)
            .map { Any() }
            .doOnNext { needsRefetch = true }
    }

    private fun toTask(task: AllTasksQuery.AllTask): Task {
        return Task(task.id(), task.name(), task.note(), task.isDone)
    }
}