package com.example.taskmanagerreallyquick.repository

interface TokenProvider {

    fun hasToken(): Boolean

    fun getToken(): String

    fun setToken(accessToken: String)

}