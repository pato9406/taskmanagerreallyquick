package com.example.taskmanagerreallyquick.repository

class TokenProviderImpl: TokenProvider {
    var userToken: String? = null

    override fun setToken(accessToken: String) {
        userToken = accessToken
    }

    override fun getToken(): String {
        return userToken ?: throw IllegalStateException("Token cannot be null")
    }

    override fun hasToken(): Boolean {
        return userToken != null
    }
}