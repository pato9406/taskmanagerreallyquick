package com.example.taskmanagerreallyquick.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.taskmanagerreallyquick.R
import com.example.taskmanagerreallyquick.TaskManagerApplication
import com.example.taskmanagerreallyquick.create.AddNewTaskActivity
import com.example.taskmanagerreallyquick.login.LoginActivity
import com.example.taskmanagerreallyquick.model.*
import com.example.taskmanagerreallyquick.model.exceptions.NotLoggedInException
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity : AppCompatActivity() {

    private var hasTasks = false
    private lateinit var viewModel: HomeViewModel
    private val onTaskCheckedListener = object : OnTaskCheckedListener {
        override fun onTaskChecked(task: Task, checked: Boolean) {
            viewModel.updateTask(task, checked)
        }
    }

    private val pendingAdapter by lazy {
        TasksAdapter(onTaskCheckedListener)
    }
    private val doneAdapter by lazy {
        TasksAdapter(onTaskCheckedListener)
    }

    private val observer: Observer<ApiRequestStatus<List<Task>>> = Observer { status ->
        when (status) {
            is Success -> displayTasks(status)
            is Failure -> handleError(status)
            is Loading -> displayLoading()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders
            .of(this, (applicationContext as TaskManagerApplication).viewModelFactory)
            .get(HomeViewModel::class.java)

        viewModel.tasks.observe(this, observer)

        pending_recycler.adapter = pendingAdapter
        done_recycler.adapter = doneAdapter

        fab.setOnClickListener {
            goToAddScreen()
        }
    }

    override fun onResume() {
        super.onResume()

        viewModel.getTasks()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestCode.Login.code && resultCode == RESULT_OK) {
            ViewModelProviders
                .of(this, (applicationContext as TaskManagerApplication).viewModelFactory)
                .get(HomeViewModel::class.java)
                .getTasks()
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun goToAddScreen() {
        startActivity(Intent(this, AddNewTaskActivity::class.java))
    }

    private fun displayTasks(status: Success<List<Task>>) {
        loading.visibility = View.GONE
        hasTasks = true

        val pendingTasks = status.results.filter { !it.done }
        if (pendingTasks.isEmpty()) {
            pending_title.visibility = View.GONE
            pending_recycler.visibility = View.GONE
        } else {
            pending_title.visibility = View.VISIBLE
            pending_recycler.visibility = View.VISIBLE
        }

        pendingAdapter.tasks = pendingTasks
        pendingAdapter.notifyDataSetChanged()

        val doneTasks = status.results.filter { it.done }
        if (doneTasks.isEmpty()) {
            done_title.visibility = View.GONE
            done_recycler.visibility = View.GONE
        }  else {
            done_title.visibility = View.VISIBLE
            done_recycler.visibility = View.VISIBLE
        }
        doneAdapter.tasks = doneTasks
        doneAdapter.notifyDataSetChanged()

        if (doneTasks.isEmpty() && pendingTasks.isEmpty()) {
            empty_screen_placeholder.visibility = View.VISIBLE
        } else {
            empty_screen_placeholder.visibility = View.GONE
        }
    }

    private fun displayLoading() {
        if (!hasTasks) {
            loading.visibility = View.VISIBLE
        }
    }

    private fun handleError(status: Failure<List<Task>>) {
        if (status.throwable is NotLoggedInException) {
            startActivityForResult(Intent(this, LoginActivity::class.java), RequestCode.Login.code)
        } else {
            Log.e("HomeActivity", "Error fetching tasks", status.throwable)
            Toast.makeText(this, "Error fetching tasks", Toast.LENGTH_SHORT).show()
        }
    }
}
