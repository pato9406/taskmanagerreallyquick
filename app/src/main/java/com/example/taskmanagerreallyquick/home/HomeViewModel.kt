package com.example.taskmanagerreallyquick.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.taskmanagerreallyquick.model.*
import com.example.taskmanagerreallyquick.model.exceptions.NotLoggedInException
import com.example.taskmanagerreallyquick.repository.TasksRepository
import com.example.taskmanagerreallyquick.repository.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class HomeViewModel(
    private val userRepository: UserRepository,
    private val tasksRepository: TasksRepository
) : ViewModel() {

    private val subscriptions = CompositeDisposable()
    val tasks = MutableLiveData<ApiRequestStatus<List<Task>>>()

    override fun onCleared() {
        super.onCleared()
        subscriptions.clear()
    }

    fun getTasks() {
        if (userRepository.isLoggedIn()) {
            fetchTasks()
        } else {
            tasks.value = Failure(NotLoggedInException())
        }
    }

    private fun fetchTasks() {
        Log.d("HomeViewModel", "Fetching things!")
        tasks.value = Loading()
        subscriptions.add(
            tasksRepository.getTasksIfNeeded()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        Log.d("HomeViewModel", "Success!")
                        tasks.value = Success(it)
                    },
                    { tasks.value = Failure(it) }
                )
        )
    }

    fun updateTask(task: Task, checked: Boolean) {
        Log.d("HomeViewModel", "Updating task!")
        subscriptions.add(
            tasksRepository.updateTask(task.id, checked)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        Log.d("HomeViewModel", "Task updated!")
                        fetchTasks()
                    },
                    { Log.e("HomeViewModel", "Error updating task", it) }
                )
        )
    }
}