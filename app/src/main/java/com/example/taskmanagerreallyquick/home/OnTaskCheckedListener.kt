package com.example.taskmanagerreallyquick.home

import com.example.taskmanagerreallyquick.model.Task

interface OnTaskCheckedListener {

    fun onTaskChecked(task: Task, checked: Boolean)

}