package com.example.taskmanagerreallyquick.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.taskmanagerreallyquick.R
import com.example.taskmanagerreallyquick.model.Task

import kotlinx.android.synthetic.main.list_item_task.view.*

class TasksAdapter(private val onTaskCheckedListener: OnTaskCheckedListener): RecyclerView.Adapter<TasksAdapter.TaskViewHolder>() {

    var tasks: List<Task> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_task, parent, false)

        return TaskViewHolder(view, onTaskCheckedListener)
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(tasks[position])
    }

    class TaskViewHolder(view: View, val onTaskCheckedListener: OnTaskCheckedListener): RecyclerView.ViewHolder(view) {
        fun bind(task: Task) {
            itemView.name.text = task.name
            itemView.notes.text = task.note
            itemView.check.isChecked = task.done

            itemView.setOnClickListener {
                onTaskCheckedListener.onTaskChecked(task, !task.done)
            }
        }
    }
}
