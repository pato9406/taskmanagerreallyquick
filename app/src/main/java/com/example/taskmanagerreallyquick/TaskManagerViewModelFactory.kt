package com.example.taskmanagerreallyquick

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.taskmanagerreallyquick.create.AddNewTaskViewModel
import com.example.taskmanagerreallyquick.home.HomeViewModel
import com.example.taskmanagerreallyquick.login.LoginViewModel
import com.example.taskmanagerreallyquick.networking.ApolloClientBuilder
import com.example.taskmanagerreallyquick.repository.TasksRepositoryImpl
import com.example.taskmanagerreallyquick.repository.TokenProviderImpl
import com.example.taskmanagerreallyquick.repository.UserRepositoryImpl

class TaskManagerViewModelFactory: ViewModelProvider.Factory {

    private val tokenProvider by lazy { TokenProviderImpl() }
    private val apolloClient by lazy { ApolloClientBuilder(tokenProvider).build() }
    private val tasksRepository by lazy { TasksRepositoryImpl(apolloClient) }
    private val userRepository by lazy { UserRepositoryImpl(tokenProvider, apolloClient) }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            HomeViewModel::class.java -> HomeViewModel(userRepository, tasksRepository) as T
            LoginViewModel::class.java -> LoginViewModel(userRepository) as T
            AddNewTaskViewModel::class.java -> AddNewTaskViewModel(tasksRepository) as T
            else -> throw NotImplementedError("Can't build view model from class $modelClass")
        }
    }
}