package com.example.taskmanagerreallyquick.model.exceptions

class NotLoggedInException: Throwable()