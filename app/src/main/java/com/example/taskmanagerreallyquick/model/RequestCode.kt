package com.example.taskmanagerreallyquick.model

enum class RequestCode(val code: Int) {
    Login(1001)
}