package com.example.taskmanagerreallyquick.model

sealed class ApiRequestStatus<T>

class Failure<T>(val throwable: Throwable): ApiRequestStatus<T>()
class Success<T>(val results: T): ApiRequestStatus<T>()
class Loading<T>: ApiRequestStatus<T>()
