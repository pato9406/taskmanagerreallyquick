package com.example.taskmanagerreallyquick.model

data class Task(
    val id: String,
    val name: String,
    val note: String?,
    val done: Boolean
)