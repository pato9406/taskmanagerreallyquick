package com.example.taskmanagerreallyquick.networking

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.interceptor.ApolloInterceptor
import com.apollographql.apollo.interceptor.ApolloInterceptorChain
import com.apollographql.apollo.request.RequestHeaders
import com.example.taskmanagerreallyquick.repository.TokenProvider
import java.util.concurrent.Executor

class ApolloClientBuilder(private val tokenProvider: TokenProvider) {
    fun build(): ApolloClient {
        return ApolloClient.builder()
            .serverUrl("https://380odjc5vi.execute-api.us-east-1.amazonaws.com/dev/graphql")
            .addApplicationInterceptor(AddAuthorization(tokenProvider))
            .build()
    }
}

class AddAuthorization(private val tokenProvider: TokenProvider) : ApolloInterceptor {
    override fun dispose() {
    }

    override fun interceptAsync(
        request: ApolloInterceptor.InterceptorRequest,
        chain: ApolloInterceptorChain,
        dispatcher: Executor,
        callBack: ApolloInterceptor.CallBack
    ) {
        if (!tokenProvider.hasToken()) {
            chain.proceedAsync(request, dispatcher, callBack)
        } else {
            val newHeaders =
                RequestHeaders.builder().addHeader("Authorization", tokenProvider.getToken())
                    .build()
            val newRequest = request.toBuilder()
                .requestHeaders(newHeaders)
                .build()
            chain.proceedAsync(newRequest, dispatcher, callBack)
        }
    }

}