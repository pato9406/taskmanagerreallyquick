package com.example.taskmanagerreallyquick.create

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.taskmanagerreallyquick.R
import com.example.taskmanagerreallyquick.TaskManagerApplication
import com.example.taskmanagerreallyquick.model.ApiRequestStatus
import com.example.taskmanagerreallyquick.model.Failure
import com.example.taskmanagerreallyquick.model.Loading
import com.example.taskmanagerreallyquick.model.Success
import kotlinx.android.synthetic.main.activity_add_new_task.*

class AddNewTaskActivity : AppCompatActivity() {

    private val observer: Observer<ApiRequestStatus<Any>> = Observer {
        when (it) {
            is Success -> goBackToHome()
            is Failure -> displayError(it)
            is Loading -> displayLoading()
        }
    }

    private lateinit var viewModel: AddNewTaskViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_task)

        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel = ViewModelProviders.of(this, (applicationContext as TaskManagerApplication).viewModelFactory)
            .get(AddNewTaskViewModel::class.java)
        viewModel.result.observe(this, observer)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_add_task, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.confirm -> callCreateTask()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun callCreateTask() {
        viewModel.createTask(name.text.toString(), notes.text.toString())
    }

    private fun goBackToHome() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun displayError(failure: Failure<Any>) {
        Log.d("AddNewTaskActivity", "Error")
    }

    private fun displayLoading() {
        Log.d("AddNewTaskActivity", "Loading")
    }
}
