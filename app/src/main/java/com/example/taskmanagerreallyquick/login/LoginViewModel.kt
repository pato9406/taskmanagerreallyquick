package com.example.taskmanagerreallyquick.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.taskmanagerreallyquick.model.ApiRequestStatus
import com.example.taskmanagerreallyquick.model.Failure
import com.example.taskmanagerreallyquick.model.Loading
import com.example.taskmanagerreallyquick.model.Success
import com.example.taskmanagerreallyquick.repository.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class LoginViewModel(private val userRepository: UserRepository) : ViewModel() {

    private val subscriptions = CompositeDisposable()
    val result = MutableLiveData<ApiRequestStatus<Any>>()

    fun login(username: String) {
        result.value = Loading()

        subscriptions.add(
            userRepository.createAccessToken(
                username
            )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result.value = Success(Any()) },
                    { result.value = Failure(it) }
                )
        )

    }
}