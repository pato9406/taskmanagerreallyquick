package com.example.taskmanagerreallyquick.login

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.taskmanagerreallyquick.R
import com.example.taskmanagerreallyquick.TaskManagerApplication
import com.example.taskmanagerreallyquick.model.ApiRequestStatus
import com.example.taskmanagerreallyquick.model.Failure
import com.example.taskmanagerreallyquick.model.Loading
import com.example.taskmanagerreallyquick.model.Success
import kotlinx.android.synthetic.main.activity_login.*
import android.content.Context
import android.view.inputmethod.InputMethodManager


class LoginActivity : AppCompatActivity() {

    private val observer: Observer<ApiRequestStatus<Any>> = Observer { status ->
        when (status) {
            is Success -> goBackToHome()
            is Failure -> displayError(status)
            is Loading -> displayLoading()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val viewModel = ViewModelProviders
            .of(this, (applicationContext as TaskManagerApplication).viewModelFactory)
            .get(LoginViewModel::class.java)

        viewModel.result.observe(this, observer)

        sign_in.setOnClickListener {
            viewModel.login(username.text.toString())
        }
    }

    private fun goBackToHome() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun displayError(failure: Failure<Any>) {
        Toast.makeText(this, "Error in login", Toast.LENGTH_SHORT).show()
        Log.e("LoginActivity", "Error in login", failure.throwable)
        loading.visibility = View.GONE
        username.visibility = View.VISIBLE
        sign_in.visibility = View.VISIBLE
    }

    private fun displayLoading() {
        loading.visibility = View.VISIBLE
        username.visibility = View.GONE
        sign_in.visibility = View.GONE
        hideKeyboard()
    }

    private fun hideKeyboard() {
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
            sign_in.windowToken,
            0
        )
    }
}
