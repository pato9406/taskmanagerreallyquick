package com.example.taskmanagerreallyquick

import android.app.Application

class TaskManagerApplication: Application() {

    val viewModelFactory by lazy { TaskManagerViewModelFactory() }

}