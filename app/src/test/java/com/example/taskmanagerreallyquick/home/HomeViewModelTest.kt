package com.example.taskmanagerreallyquick.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.taskmanagerreallyquick.repository.TasksRepository
import com.example.taskmanagerreallyquick.repository.UserRepository
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito.*

class HomeViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var viewModel: HomeViewModel
    lateinit var userRepository: UserRepository
    lateinit var tasksRepository: TasksRepository

    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        userRepository = mock(UserRepository::class.java)
        tasksRepository = mock(TasksRepository::class.java)
        viewModel = HomeViewModel(userRepository, tasksRepository)
    }

    @After
    fun tearDown() {
        RxAndroidPlugins.reset()
    }

    @Test
    fun fetchsInfoIfNeeded_onLoggedIn_callsGetTasks() {
        given(userRepository.isLoggedIn()).willReturn(true)
        given(tasksRepository.getTasksIfNeeded()).willReturn(Observable.just(emptyList()))

        viewModel.getTasks()

        verify(tasksRepository).getTasksIfNeeded()
    }

    @Test
    fun fetchsInfoIfNeeded_onNotLoggedIn_doesntCallsGetTasks() {
        given(userRepository.isLoggedIn()).willReturn(false)
        given(tasksRepository.getTasksIfNeeded()).willReturn(Observable.just(emptyList()))

        viewModel.getTasks()

        verify(tasksRepository, never()).getTasksIfNeeded()
    }
}